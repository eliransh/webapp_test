# VERSION 0.2
# DOCKER-VERSION 0.3.4
# To build:
# 1. Install docker (http://docker.io)
# 2. Checkout source: git@github.com:gasi/docker-node-hello.git
# 3. Build container: docker build .

FROM    centos:6

# Enable EPEL for Node.js
RUN     rpm -Uvh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
# Install Node.js and npm
RUN     yum install -y npm

# App
ADD . /node_test
# Install app dependencies
RUN cd /node_test; npm install
RUN cd /node_test; npm install -g grunt grunt-cli
RUN ls -l /node_test
#RUN cd /node_test; npm install

EXPOSE  3000

WORKDIR /node_test
CMD ["grunt"]
