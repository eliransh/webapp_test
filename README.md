# node_test [![Build Status](https://secure.travis-ci.org/someuser/node-test.png?branch=master)](https://travis-ci.org/someuser/node-test)

A [Node.js](http://nodejs.org) module.




## Getting Started

### To run

```
$ node index.js
```

## License

[MIT License](http://en.wikipedia.org/wiki/MIT_License)